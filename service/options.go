package service

import (
	"flag"
	"fmt"
	//"time"
)

type Options struct {

	PidPath string

	EtcdNodes       listOptions
	CassandraNodes  listOptions
  ZookeeperNodes  listOptions
  
  CassandraKeyspace string
  ListenAddr      string
  IfaceName       string
  
  
}


// Helper to parse options that can occur several times, e.g. cassandra nodes
type listOptions []string

func (o *listOptions) String() string {
	return fmt.Sprint(*o)
}

func (o *listOptions) Set(value string) error {
	*o = append(*o, value)
	return nil
}

func ParseCommandLine() (options Options, err error) {
	flag.Var(&options.EtcdNodes, "peers", "Etcd Endpoints")
	flag.Var(&options.CassandraNodes, "cassandra", "Cassandra contact points")
	flag.StringVar(&options.CassandraKeyspace, "keyspace","xcms", "Cassandra keyspace")
  
	flag.Var(&options.ZookeeperNodes, "zookeeper", "Zookeeper server(s)")
  
  flag.StringVar(&options.IfaceName, "interface","eth0", "Interface to draw address from")

	flag.Parse()
  // options, err = validateOptions(options)
  // if err != nil {
  //   return options, err
  // }
	return options, nil
}