package service

import (
	//"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
  "net"
	"os/signal"
	"syscall"
  "time"
  log "github.com/Sirupsen/logrus"
  "github.com/coreos/go-etcd/etcd"
  "github.com/gocql/gocql"
  "bitbucket.org/xpandmmi/websockets/engine"
  "bitbucket.org/xpandmmi/websockets/handlers"
  "github.com/samuel/go-zookeeper/zk"
  "github.com/facebookgo/inject"
  "bitbucket.org/xpandmmi/websockets/services/kafka"
)


func Run() error {
	options, err := ParseCommandLine()
	if err != nil {
		return fmt.Errorf("failed to parse command line: %s", err)
	}
	service := NewService(options)
  
  log.SetOutput(os.Stderr)
  log.SetLevel(log.DebugLevel)
   
	if err := service.Start(); err != nil {
    log.WithFields(log.Fields{
      "error": "execution_failed",
      "msg": err,
    }).Error("The execution has failed")
    
		return fmt.Errorf("execution failure: %s", err)
	} else {
	}
  return nil
  
}



type Service struct {
	options       Options
	errorC        chan error
	sigC          chan os.Signal
  app           handlers.AppLoader
}

func NewService(options Options) *Service {
	return &Service{
		options:  options,
		errorC:   make(chan error),
		// Channel receiving signals has to be non blocking, otherwise the service can miss a signal.
		sigC: make(chan os.Signal, 1024),
	}
}

func getIPv4ForInterfaceName(ifname string) (ifaceip string) {
	interfaces, _ := net.Interfaces()
	for _, inter := range interfaces {
		if inter.Name == ifname {
			if addrs, err := inter.Addrs(); err == nil {
				for _, addr := range addrs {
					switch ip := addr.(type) {
					case *net.IPNet:
						if ip.IP.DefaultMask() != nil {
							return (ip.IP.String())
						}
					}
				}
			}
		}
	}
	return ("")
}

func (s *Service) cassandraService() *gocql.Session {
  cluster := gocql.NewCluster(s.options.CassandraNodes...)
  cluster.Keyspace = "xcms"
  cluster.Timeout = 1 * time.Minute
  
  cluster.Consistency = gocql.One
  cluster.Compressor = gocql.SnappyCompressor{}
  
  session, err := cluster.CreateSession()
  //defer session.Close()
  if err != nil {
    log.WithFields(log.Fields{
      "err": err,
    }).Error("Error communicating with cassandra")
    panic(err)
  }
  return session
}

func (s *Service) zookeeperService() *zk.Conn {
  if len(s.options.ZookeeperNodes) == 0{
    log.Error("No ZK Nodes")
    panic("")
  }
  c, _, err := zk.Connect(s.options.ZookeeperNodes, time.Second) //*10)
  if err != nil {
    log.WithFields(log.Fields{
      "err": err,
    }).Error("Error communicating with zookeeper")
    panic(err)
  }
  return c
}


func (s *Service) Start() error {

	if s.options.PidPath != "" {
		ioutil.WriteFile(s.options.PidPath, []byte(fmt.Sprint(os.Getpid())), 0644)
	}
  
  zookeeperService := s.zookeeperService()
  kafkaService := kafka.NewKafka(zookeeperService)
  err := inject.Populate(s.cassandraService(), zookeeperService ,kafkaService, &s.app)
  
  if err != nil {
    log.WithFields(log.Fields{
      "err": err,
    }).Error("Error injecting components")
    return nil
  }
  
  if ifname := getIPv4ForInterfaceName(s.options.IfaceName); ifname != "" {

  	if err := s.registerService(ifname); err != nil {
  		return err
  	}
    if err := s.newEngine(ifname); err != nil {
      return err
    }
  } else {
    log.WithFields(log.Fields{
      "interface": s.options.IfaceName,
    }).Error("The interface was not found on this system")
    return fmt.Errorf("Failure booting system")
  }
  

	signal.Notify(s.sigC, os.Interrupt, os.Kill, syscall.SIGTERM, syscall.SIGUSR2, syscall.SIGCHLD)

  return nil
	// Block until a signal is received or we got an error

}

func (s *Service) newEngine(iface string) error {
  
  if err := engine.Run(iface, s.app); err != nil {
    return err
  }
  
  return nil
}

func (s *Service) registerService(iface string) error {
  client := etcd.NewClient(s.options.EtcdNodes)
  if _, err := client.Set("/vulcand/backends/adminbe/backend", "{\"Type\": \"http\"}", 0); err != nil {
      return fmt.Errorf("Failure setting backend type:\n %s", err)
  } else {
    if _, err := client.Set("/vulcand/backends/adminbe/servers/srv1", "{\"URL\": \"http://"+iface+":3000/\"}", 0); err != nil {
      return fmt.Errorf("Failure adding backend node:\n %s", err)
    
    }else {
      if _, err := client.Set("/vulcand/frontends/adminfe/frontend", "{\"Type\": \"http\", \"BackendId\": \"adminbe\", \"Route\": \"PathRegexp(`/admin/(.*?)`)\"}", 0); err != nil {
        return fmt.Errorf("Failure adding backend node:\n %s", err)
      }
    }
  }
  return nil
}