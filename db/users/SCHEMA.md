# Users Table Schema

### CQL

```
CREATE TABLE c2s2.users (
    id timeuuid,
    username text,
    email text,
    birthday timestamp,
    createdat timestamp,
    password text,
    realname text,
    twittername text,
    usertype int,
    image text,
    PRIMARY KEY ((id, username), email)
) WITH CLUSTERING ORDER BY (email ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = '{"keys":"ALL", "rows_per_partition":"NONE"}'
    AND comment = 'SELECT id, username, password, email, twittername, realname, birthday, createdat, rank FROM users'
    AND compaction = {'min_threshold': '4', 'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32'}
    AND compression = {'sstable_compression': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99.0PERCENTILE';

```

### JSON

**Single User Request**
```json
{id: '', username: '', realname: '', twittername: '', ...}
```
Please keep in mind that no password (cleartext or encrypted will be submitted)

---

**Multiple Users**

Multiple users gets send as single user object, due to streaming.
  
  
```json
{usr1}
...
{usrN}
```
  
---




### ROUTES


#### Requests

**get users** (authenticated) get all the users

**perform login** (unauthenticated) try to login the request


#### Responses
