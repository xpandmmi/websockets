package users

import (
  "github.com/gocql/gocql"
  "golang.org/x/crypto/bcrypt"
)

type User struct {
	Id          gocql.UUID  `json:"id"`
  Username    string      `json:"username"`
  Email       string      `json:"email"`
  Realname    string      `json:"realname"`
  Password    string      `json:"-"`
  Twittername string      `json:"twittername"`
}

func NewUser(id gocql.UUID, username string, email string, realname string, password string, twittername string) *User {
	return &User{
		Id: id,
    Username: username,
    Email: email,
    Realname: realname,
    Password: password,
    Twittername: twittername,
	}
}


func (u *User) SetPassword(password string) error {
  hpass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
  if err != nil {
    return err
  }
  u.Password = string( hpass )
  return nil
}