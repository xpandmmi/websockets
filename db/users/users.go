package users

import (
	"encoding/json"
  log "github.com/Sirupsen/logrus"
  "bitbucket.org/xpandmmi/websockets/handlers"
)
type Users struct {
  loader *handlers.AppLoader
}

func NewUsers(loader *handlers.AppLoader) * Users {
  return &Users{
    loader: loader,
  }
}

func (u *Users) GetAll(fn func([]byte)) {
  //users := []User{}
  user := User{}
  log.Debug("Getting all users")
  iter := u.loader.CassandraSession.Query(`SELECT id, username, email, realname, twittername FROM users`).Iter()
  for iter.Scan(&user.Id, &user.Username, &user.Email, &user.Realname, &user.Twittername) {
    log.WithFields(log.Fields{
      "user": user,
    }).Info("User output")
    
      //
    userJson, err := json.Marshal(user)
    if err != nil {
      log.WithFields(log.Fields{
        "error": err,
      }).Error("User output error")
      fn([]byte{})
       //u.socket.Emit("error", err)
       return
    }
    fn(userJson)
      // //u.socket.Emit("user", string(b))

  }
  if err := iter.Close(); err != nil {
    //u.socket.Emit("error", err)
  }
}

func (u *Users) GetSingle(usr string, fn func(string)){
  
}

func (u *Users) Login(message string){
  //u.socket.Emit("auth response", "NYI")
}


// user


