# Files Table Schema

### CQL

```
CREATE TABLE c2s2.files (
    id uuid,
    created timestamp,
    data blob,
    filename text,
    filesize int,
    mimetype text,
    PRIMARY KEY (id, created)
) WITH CLUSTERING ORDER BY (created ASC)
    AND bloom_filter_fp_chance = 0.01
    AND caching = '{"keys":"ALL", "rows_per_partition":"NONE"}'
    AND comment = ''
    AND compaction = {'min_threshold': '4', 'class': 'org.apache.cassandra.db.compaction.SizeTieredCompactionStrategy', 'max_threshold': '32'}
    AND compression = {'sstable_compression': 'org.apache.cassandra.io.compress.LZ4Compressor'}
    AND dclocal_read_repair_chance = 0.1
    AND default_time_to_live = 0
    AND gc_grace_seconds = 864000
    AND max_index_interval = 2048
    AND memtable_flush_period_in_ms = 0
    AND min_index_interval = 128
    AND read_repair_chance = 0.0
    AND speculative_retry = '99.0PERCENTILE';

INSERT INTO c2s2.files (id, created, data, filename, filesize) VALUES(1a9bc1ff-b06f-4ab7-96a2-a9cea12e9213, dateof(now()), textAsBlob('bdb14fbe076f6b94444c660e36a400151f26fc6f'), 'textasblob.txt', 123);
```

### JSON


### ROUTES


#### Requests

**GET /file/:uuid/:filename** get a single file

**POST /files/upload** uploads multiple files (formData: files[])

#### Responses
