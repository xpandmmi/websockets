package files

import (
	"encoding/json"
  log "github.com/Sirupsen/logrus"
  "github.com/gocql/gocql"
  "net/http"
  "github.com/gorilla/mux"
  //"time"
  "strconv"
  "time"
  "io/ioutil"
  "bitbucket.org/xpandmmi/websockets/handlers"
  
  
)
type Files struct {
  loader   *handlers.AppLoader
}

func NewFiles(loader *handlers.AppLoader) * Files {
  return &Files{
    loader: loader,
  }
}

func (f *Files) ServeUpload(w http.ResponseWriter, r *http.Request) {
  
	if r.Method != "POST" {
		http.Error(w, "Method not allowed", 405)
		return
	}
  w.Header().Set("Content-Type", "application/json")
  
	err := r.ParseMultipartForm(1000000)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//get a ref to the parsed multipart form
	m := r.MultipartForm
  files := m.File["files"]

	for i, _ := range files {
    file, err := files[i].Open()
    defer file.Close()
    if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
    ruuid, err := gocql.RandomUUID()
    tstamp := time.Now().UTC().Unix()
    fbyte, err := ioutil.ReadAll(file)
    if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
    filename := files[i].Filename
    
    log.WithFields(log.Fields{
      "timestamp": tstamp,
      "uuid": ruuid.String(),
    }).Info("File Upload")
    
    if err := f.loader.CassandraSession.Query(`INSERT INTO files (id, created, data, filename, filesize, mimetype) VALUES (?, ?, ?, ?, ?, 'application/octet-stream')`,
        ruuid, tstamp, fbyte, filename, int64(len(fbyte))).Exec(); err != nil {
    			http.Error(w, "cql:"+err.Error(), http.StatusInternalServerError)
          return
    }

    fle := File{
      Id: ruuid, 
      Filename: filename, 
      MimeType: "none", 
      Filesize: int64(len(fbyte)), 
      Created: tstamp,
      Url: "/files/"+strconv.FormatInt(tstamp, 10)+"/"+ruuid.String()+"/"+filename,
    }

    js, err := json.Marshal(fle)
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    w.Write(js)

  }
}

func (s *Files) ServeDownload(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  log.WithFields(log.Fields{
    "vars": vars,
  }).Info("File Request")
  
  var data []byte
  var size int64
  var name string
  var mimetype string
  var created time.Time
  timestamp, err := strconv.ParseInt(vars["created"], 10, 64)
  if err != nil {
    http.Error(w, err.Error(), http.StatusBadRequest)
    return
  }
  
  
  
  log.WithFields(log.Fields{
    "timestamp": timestamp,
    "uuid": vars["uuid"],
  }).Info("File Download")
  if err := s.loader.CassandraSession.Query(`select filename, filesize, data, created, mimetype from files where created=? AND id=? LIMIT 1`, timestamp, vars["uuid"]).Scan(&name, &size, &data, &created, &mimetype); err != nil {
    http.Error(w, err.Error(), http.StatusNotFound)
    return
  }
  
  w.Header().Set("Content-Type", mimetype)
  
  w.Header().Set("Vary", "Accept-Encoding")
  w.Header().Set("Pragma", "public")
  w.Header().Set("Content-Length", strconv.FormatInt(size, 10))
  w.Header().Set("Last-Modified", created.Format(time.RFC1123))  
  w.Write(data)
  
}