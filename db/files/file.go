package files

import (
  "github.com/gocql/gocql"
)

type File struct {
	Id          gocql.UUID  `json:"id"`
  Filename    string      `json:"name"`
  MimeType    string      `json:"mime"`
  Content     []byte      `json:"-"`
  Filesize    int64       `json:"size"`
  Url         string      `json:"url"`
  Created     int64       `json:"created"`
}

func NewFile(id gocql.UUID, filename string, mimetype string, content []byte, filesize int64) *File {
	return &File{
		Id: id,
    Filename: filename,
    MimeType: mimetype,
    Content: content,
    Filesize: filesize,
	}
}
