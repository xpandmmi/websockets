package tweets

import (
  "github.com/gocql/gocql"
  "time"
  "bytes"
  "github.com/stealthly/go-avro"
)
//id, posted, parsed, favs, retweets, username

type Tweet struct {
	Id          gocql.UUID  `json:"id"`
  Posted      time.Time   `json:"posted"`
  Parsed      string      `json:"parsed"`
  Favs        string      `json:"favs"`
  Retweets    string      `json:"retweets"`
  Username    string      `json:"username"`
}

type MinTweet struct {
  Content     string      `json:"message"`
  User        string      `json:"user"`
  UserUUID    string      `json:"userid"`
	encoded []byte
	err     error
}


func (t *MinTweet) AvroScheme() string {
  return `{
  "namespace": "com.xpand.avro",
  "type": "record",
  "name": "Tweet",
  "fields": [
      {
        "name": "timestamp", 
        "type": "long",
        "doc": "Server timestamp the tweet was sent"
      },
      {
        "name": "content", 
        "type": "string",
        "doc": "Content of the tweet in raw format"
      },
      {
        "name": "user", 
        "type": "string",
        "doc": "User identification. Will be matched against the database"
      },
      {
        "name": "userid", 
        "type": "string",
        "doc": "User UUID"
      }
  ]
}`
}


func (t *MinTweet) ensureEncoded() {
	schema := avro.MustParseSchema(t.AvroScheme())
  record := avro.NewGenericRecord(schema)
  
  record.Set("timestamp", time.Now().Unix())
  record.Set("content", t.Content)
  record.Set("user", t.User)
  record.Set("userid", t.UserUUID)
  
  writer := avro.NewGenericDatumWriter()
  writer.SetSchema(schema)
	buffer := new(bytes.Buffer)
	encoder := avro.NewBinaryEncoder(buffer)
  err := writer.Write(record, encoder)
	if err != nil {
		t.err = err
	}
  
	if t.encoded == nil && t.err == nil {
		t.encoded = buffer.Bytes()
	}
}

func (t *MinTweet) Length() int {
	t.ensureEncoded()
	return len(t.encoded)
}

func (t *MinTweet) Encode() ([]byte, error) {
	t.ensureEncoded()
	return t.encoded, t.err
}