package tweets

import (
	"encoding/json"
  "bitbucket.org/xpandmmi/websockets/handlers"
  "bitbucket.org/xpandmmi/websockets/responses"
  //log "github.com/Sirupsen/logrus"
  
)

type Twitter struct {
  loader *handlers.AppLoader
}

func NewTwitter(handler *handlers.AppLoader) *Twitter {
  return &Twitter{
    loader: handler,
  }
}


func (t *Twitter) PostTweet(event string, payload []byte, cb func([]byte)) {
  minTweet := &MinTweet{}
  if err := json.Unmarshal(payload, &minTweet); err != nil {
     cb(responses.ErrorEvent(err))
     panic(err)
     return
  }
  t.loader.Kafkas.PublishSync("tweets", minTweet)
  cb(responses.KafkaEnqueued())
}


func (t *Twitter) LatestTweets(num int, cb func([]byte)){
  tweet := &Tweet{}
  iter := t.loader.CassandraSession.Query(`SELECT id, posted, parsed, favs, retweets, username FROM twitter`).Iter()
  for iter.Scan(&tweet.Id, &tweet.Posted, &tweet.Parsed, &tweet.Favs, &tweet.Retweets, &tweet.Username) {
    userJson, err := json.Marshal(tweet)
    if err != nil {
      cb(responses.ErrorEvent(err))
      return
    }
    cb(userJson)

  }
  if err := iter.Close(); err != nil {
    cb(responses.ErrorEvent(err))
  }
}