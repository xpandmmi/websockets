'use strict';

/**
 * @ngdoc overview
 * @name xmcsApp
 * @description
 * # xmcsApp
 *
 * Main module of the application.
 */
angular
  .module('xmcsApp', [
    'ngWebsocket',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'picardy.fontawesome',
    'ui.bootstrap',
    'ui.router',
    'ui.utils',
    'angular-loading-bar',
    'angular-momentjs',
    'lazyModel',
    'toastr',
    'angularBootstrapNavTree',
    'oc.lazyLoad',
    'ui.select',
    'ui.tree',
    'textAngular',
    'colorpicker.module',
    'angularFileUpload',
    'ngImgCrop',
    'datatables',
    'datatables.bootstrap',
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.edit',
    'ui.grid.moveColumns',
    'ngTable',
    'smart-table',
    'angular-flot',
    'angular-rickshaw',
    'easypiechart',
    'ui.calendar'
  ])
  .factory('websockets', ['$websocket', function apiTokenFactory($websocket) {
    var ws = $websocket.$new({
        url: 'ws://localhost:3000/ws',
        protocols: ["websocket"],
        lazy: true,
        enqueue: true,
        reconnect: true,
        reconnectInterval: 500
    });
    
    ws.$open(); 

    return ws;
  }])
  .run(['$rootScope', '$state', '$stateParams', 'websockets', function($rootScope, $state, $stateParams, websockets) {
    // when the websocket gets open, flushes every message stored in the internal queue
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function(event, toState) {

      event.targetScope.$watch('$viewContentLoaded', function () {

        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);

        setTimeout(function () {
          angular.element('#wrap').css('visibility','visible');

          if (!angular.element('.dropdown').hasClass('open')) {
            angular.element('.dropdown').find('>ul').slideUp();
          }
        }, 200);
      });
      $rootScope.containerClass = toState.containerClass;
    });
  }])

  .config(['uiSelectConfig', function (uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
  }])

  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/app/dashboard');

    $stateProvider

    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'views/tmpl/app.html'
    })
    //dashboard
    .state('app.dashboard', {
      url: '/dashboard',
      controller: 'DashboardCtrl',
      templateUrl: 'views/tmpl/dashboard.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css'
          ]);
        }]
      }
    })

    //shop
    .state('app.shop', {
      url: '/shop',
      template: '<div ui-view></div>'
    })
    //shop/orders
    .state('app.shop.orders', {
      url: '/orders',
      controller: 'OrdersCtrl',
      templateUrl: 'views/tmpl/shop/orders.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })
    //shop/products
    .state('app.shop.products', {
      url: '/products',
      controller: 'ProductsCtrl',
      templateUrl: 'views/tmpl/shop/products.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })
    //shop/invoices
    .state('app.shop.invoices', {
      url: '/invoices',
      controller: 'InvoicesCtrl',
      templateUrl: 'views/tmpl/shop/invoices.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })
    //shop/single-order
    .state('app.shop.single-order', {
      url: '/single-order',
      controller: 'SingleOrderCtrl',
      templateUrl: 'views/tmpl/shop/single-order.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })
    //shop/single-product
    .state('app.shop.single-product', {
      url: '/single-product',
      controller: 'SingleProductCtrl',
      templateUrl: 'views/tmpl/shop/single-product.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js',
            'scripts/vendor/touchspin/jquery.bootstrap-touchspin.js',
            'scripts/vendor/touchspin/jquery.bootstrap-touchspin.css',
            'scripts/vendor/magnific/magnific-popup.css',
            'scripts/vendor/magnific/jquery.magnific-popup.min.js'
          ]);
        }]
      }
    })
    //shop/single-invoice
    .state('app.shop.single-invoice', {
      url: '/single-invoice',
      controller: 'SingleInvoiceCtrl',
      templateUrl: 'views/tmpl/shop/single-invoice.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })
   
   
    .state('app.analytics', {
      url: '/analytics',
      template: '<div ui-view></div>'
    })
    .state('app.analytics.accesslog', {
      url: '/accesslog',
      controller: 'AnalyticsAccessLogCtrl',
      templateUrl: 'views/tmpl/tables/bootstrap.html'
    })
    
    .state('app.sys', {
      url: '/s',
      template: '<div ui-view></div>'
    })
    .state('app.sys.status', {
      url: '/status',
      controller: 'SystemStatusCtrl',
      templateUrl: 'views/tmpl/system/status.html'
    })
    .state('app.social', {
      url: '/soc',
      template: '<div ui-view></div>'
    })
    .state('app.social.twitter', {
      url: '/twitter',
      controller: 'SocialTwitterCtrl',
      templateUrl: 'views/tmpl/social/twitter.html'
    })
       
    //app core pages (errors, login,signup)
      .state('core', {
      abstract: true,
      url: '/core',
      template: '<div ui-view></div>'
    })
    //login
    .state('core.login', {
      url: '/login',
      controller: 'LoginCtrl',
      templateUrl: 'views/tmpl/pages/login.html'
    })
    //signup
    .state('core.signup', {
      url: '/signup',
      controller: 'SignupCtrl',
      templateUrl: 'views/tmpl/pages/signup.html'
    })

    //page 404
    .state('core.page404', {
      url: '/page404',
      templateUrl: 'views/tmpl/pages/page404.html'
    })
    //page 500
    .state('core.page500', {
      url: '/page500',
      templateUrl: 'views/tmpl/pages/page500.html'
    })
    //page offline
    .state('core.page-offline', {
      url: '/page-offline',
      templateUrl: 'views/tmpl/pages/page-offline.html'
    })
    //locked screen
    .state('core.locked', {
      url: '/locked',
      templateUrl: 'views/tmpl/pages/locked.html'
    })
    //documentation
    .state('app.help', {
      url: '/help',
      controller: 'HelpCtrl',
      templateUrl: 'views/tmpl/help.html'
    });
  }]);

