'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:SidebarsmlayoutCtrl
 * @description
 * # SidebarsmlayoutCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SidebarsmlayoutCtrl', function ($scope) {
    $scope.page = {
      title: 'Small Sidebar Layout',
      subtitle: 'Place subtitle here...'
    };
  });
