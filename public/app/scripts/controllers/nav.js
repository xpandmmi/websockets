'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('NavCtrl', function ($scope) {
    $scope.oneAtATime = false;

    $scope.status = {
      isFirstOpen: true,
      isSecondOpen: true,
      isThirdOpen: true
    };
  });