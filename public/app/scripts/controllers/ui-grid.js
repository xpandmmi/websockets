'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:UiGridCtrl
 * @description
 * # UiGridCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('GridCtrl', function ($scope) {
    $scope.page = {
      title: 'Grid',
      subtitle: 'Place subtitle here...'
    };
  });
