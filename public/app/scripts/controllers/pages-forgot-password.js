'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:PagesForgotPasswordCtrl
 * @description
 * # PagesForgotPasswordCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('ForgotPasswordCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
