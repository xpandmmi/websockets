'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:BoxedlayoutCtrl
 * @description
 * # BoxedlayoutCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('BoxedlayoutCtrl', function ($scope) {
    $scope.page = {
      title: 'Boxed Layout',
      subtitle: 'Place subtitle here...'
    };
  });
