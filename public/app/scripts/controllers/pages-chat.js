'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:PagesChatCtrl
 * @description
 * # PagesChatCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('ChatCtrl', function ($scope, $resource) {
    $scope.inbox = $resource('scripts/jsons/chats.json').query();

    $scope.archive = function(index) {
      $scope.inbox.splice(index, 1);
    };
  });
