'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:HzmenuCtrl
 * @description
 * # HzmenuCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('HzmenuCtrl', function ($scope) {
     $scope.page = {
      title: 'Horizontal menu Layout',
      subtitle: 'Place subtitle here...'
    };
  });
