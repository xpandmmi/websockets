'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:ButtonsiconsCtrl
 * @description
 * # ButtonsiconsCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('ButtonsIconsCtrl', function ($scope) {
     $scope.page = {
      title: 'Buttons & Icons',
      subtitle: 'Place subtitle here...'
    };
  });
