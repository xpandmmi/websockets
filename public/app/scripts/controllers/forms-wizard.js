'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:FormsWizardCtrl
 * @description
 * # FormsWizardCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('FormWizardCtrl', function ($scope) {
    $scope.page = {
      title: 'Form Wizard',
      subtitle: 'Place subtitle here...'
    };
  });
