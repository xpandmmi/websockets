'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:AnalyticsAccessLoGCtrl
 * @description
 * # TablesBootstrapCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SocialTwitterCtrl', ['$scope', 'toastr', 'toastrConfig', 'websockets', function ($scope, toastr, toastrConfig, websockets) {
     $scope.page = {
      title: 'Twitter',
      subtitle: 'XCMS'
    };

    var openedToasts = [];
    
    $scope.user = {};
    $scope.tweet = function(){
      websockets.$emit("POST /tweets/create", {user: $scope.user.username, userid: $scope.user.id,message: $scope.message});
    }
    
    $scope.options = {
      position: 'toast-top-right',
      type: 'success',
      timeout: '5000',
      extendedTimeout: '1000',
      html: false,
      closeButton: false,
      tapToDismiss: true,
      closeHtml: '<i class="fa fa-times"></i>'
    };

    $scope.$watchCollection('options', function(newValue) {
      toastrConfig.allowHtml = newValue.html;
      toastrConfig.extendedTimeOut = parseInt(newValue.extendedTimeout, 10);
      toastrConfig.positionClass = newValue.position;
      toastrConfig.timeOut = parseInt(newValue.timeout, 10);
      toastrConfig.closeButton = newValue.closeButton;
      toastrConfig.tapToDismiss = newValue.tapToDismiss;
      toastrConfig.closeHtml = newValue.closeHtml;
      toastrConfig.iconType = newValue.iconType;
    });
    
    $scope.successToast = function (msg, title) {
      var toast = toastr[$scope.options.type](msg, title, {
        iconClass: 'bg-success',
        iconType: "fa-check"
      });
      openedToasts.push(toast);
    }
    $scope.usrs = [];

    $scope.users = null;
    $scope.updateUsers = function(){
      $scope.usrs = [];
      websockets.$on("user", function(user){
        
        $scope.usrs.push(JSON.parse(user));
        $scope.apply();
      });
      websockets.$emit("GET /users");
    }
    
    $scope.updateUsers();

  }]);
