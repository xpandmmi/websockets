'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:TypographyCtrl
 * @description
 * # TypographyCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('TypographyCtrl', function ($scope) {
    $scope.page = {
      title: 'Typography',
      subtitle: 'Place subtitle here...'
    };
  });
