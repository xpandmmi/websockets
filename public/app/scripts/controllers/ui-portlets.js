'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:UiPortletsCtrl
 * @description
 * # UiPortletsCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('PortletsCtrl', function ($scope) {
    $scope.page = {
      title: 'Portlets',
      subtitle: 'Place subtitle here...'
    };
  });
