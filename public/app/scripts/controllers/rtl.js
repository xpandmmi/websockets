'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:RtlCtrl
 * @description
 * # RtlCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('RtlCtrl', function ($scope) {
    $scope.page = {
      title: 'RTL Layout',
      subtitle: 'Place subtitle here...'
    };
  });
