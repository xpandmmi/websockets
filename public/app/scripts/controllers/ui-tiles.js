'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:UitilesCtrl
 * @description
 * # UitilesCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('TilesCtrl', function ($scope) {
    $scope.page = {
      title: 'Tiles',
      subtitle: 'Place subtitle here...'
    };
  });
