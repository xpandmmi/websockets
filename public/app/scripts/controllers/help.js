'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:HelpCtrl
 * @description
 * # HelpCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('HelpCtrl', function ($scope) {
     $scope.page = {
      title: 'Documentation',
      subtitle: 'Place subtitle here...'
    };
  });
