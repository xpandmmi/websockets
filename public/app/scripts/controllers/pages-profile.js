'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:PagesProfileCtrl
 * @description
 * # PagesProfileCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('ProfileCtrl', function ($scope) {
    $scope.page = {
      title: 'Profile Page',
      subtitle: 'Place subtitle here...'
    };
  });
