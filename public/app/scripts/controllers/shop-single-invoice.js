'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:ShopSingleInvoiceCtrl
 * @description
 * # ShopSingleInvoiceCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SingleInvoiceCtrl', function ($scope) {
    $scope.page = {
      title: 'Single Invoice',
      subtitle: 'Place subtitle here...'
    };
  });
