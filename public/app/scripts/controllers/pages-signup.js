'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:PagesSignupCtrl
 * @description
 * # PagesSignupCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SignupCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
