'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:PagesLoginCtrl
 * @description
 * # PagesLoginCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('LoginCtrl', function ($scope, $state) {
    $scope.login = function() {
      $state.go('app.dashboard');
    };
  });
