'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:SidebarxslayoutCtrl
 * @description
 * # SidebarxslayoutCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SidebarxslayoutCtrl', function ($scope) {
    $scope.page = {
      title: 'Extra-small Sidebar Layout',
      subtitle: 'Place subtitle here...'
    };
  });
