'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:SystemStatus
 * @description
 * # DashboardCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SystemStatusCtrl', function($scope,socket){
    $scope.page = {
      title: 'System Status',
      subtitle: ''
    };
    
    $scope.getStatus = function(){
      $scope.data={};
      socket.on("status", function(stati){
        $scope.data=stati;
      });
      socket.emit("get status");
    }
    
    

    // $scope.getUsers = function(){
    //   $scope.data=[];
    //   var url = 'http://www.filltext.com/?rows=10&fname={firstName}&lname={lastName}&delay=3&callback=JSON_CALLBACK';
    //
    //   $http.jsonp(url).success(function(data){
    //       $scope.data=data;
    //   });
    // };

    $scope.getStatus();
  });

