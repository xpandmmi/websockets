'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:AnalyticsAccessLoGCtrl
 * @description
 * # TablesBootstrapCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('SocialTweetCtrl', ['$scope', 'toastr', 'toastrConfig', 'websockets', function ($scope, toastr, toastrConfig, websockets) {
    $scope.user = {};
    $scope.tweet = function(){
      websocket.$emit("post tweet", {user: $scope.user, message: $scope.message});
    }

  }]);
