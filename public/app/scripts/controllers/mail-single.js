'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:MailSingleCtrl
 * @description
 * # MailSingleCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('MailSingleCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
