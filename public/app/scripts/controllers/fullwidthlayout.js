'use strict';

/**
 * @ngdoc function
 * @name xmcsApp.controller:FullwidthlayoutCtrl
 * @description
 * # FullwidthlayoutCtrl
 * Controller of the xmcsApp
 */
angular.module('xmcsApp')
  .controller('FullwidthlayoutCtrl', function ($scope) {
    $scope.page = {
      title: 'Full-width Layout',
      subtitle: 'Place subtitle here...'
    };
  });
