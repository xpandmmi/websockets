'use strict';

/**
 * @ngdoc directive
 * @name xmcsApp.directive:vectorMap
 * @description
 * # vectorMap
 */
angular.module('xmcsApp')
  .directive('vectorMap', function () {
    return {
      restrict: 'AE',
      scope: {
        options: '='
      },
      link: function postLink(scope, element) {
        var options = scope.options;
        element.vectorMap(options);
      }
    };
  });
