FROM golang:1.4-onbuild
EXPOSE 3000

RUN make install

ADD run.sh /run.sh

ENTRYPOINT ["/run.sh"]