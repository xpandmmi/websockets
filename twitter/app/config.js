require.config({
  shim: {

  },
  paths: {
    angular: "../bower_components/angular/angular",
    "angular-animate": "../bower_components/angular-animate/angular-animate",
    "angular-cookies": "../bower_components/angular-cookies/angular-cookies",
    "angular-messages": "../bower_components/angular-messages/angular-messages",
    "angular-mocks": "../bower_components/angular-mocks/angular-mocks",
    "angular-resource": "../bower_components/angular-resource/angular-resource",
    "angular-route": "../bower_components/angular-route/angular-route",
    "angular-sanitize": "../bower_components/angular-sanitize/angular-sanitize",
    "angular-touch": "../bower_components/angular-touch/angular-touch",
    oclazyload: "../bower_components/oclazyload/dist/ocLazyLoad"
  },
  packages: [

  ]
});
