'use strict'

###*
 # @ngdoc overview
 # @name twitterApp
 # @description
 # # twitterApp
 #
 # Main module of the application.
###
angular
  .module 'twitterApp', [
    'oc.lazyLoad',
    'ngWebsocket',
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]
  .factory 'websockets', ['$websocket', ($websocket) ->
    ws = $websocket.$new
        url: 'ws://localhost:3000/ws'
        protocols: ["websocket"]
        lazy: true
        enqueue: true
        reconnect: true
        reconnectInterval: 500
    
    ws.$open()

    ws
  ]
  .filter 'unsafe', ($sce) -> 
    return $sce.trustAsHtml
  .factory 'tweet', [ '$rootScope', 'websockets', ($rootScope, websockets) ->
    tweet = {tweet: false}
    return tweet
  ]  
  .directive "contenteditable", ->
    return{
      restrict: "A",
      require: "ngModel",
      link: (scope, element, attrs, ngModel) ->

        read = () ->
          ngModel.$setViewValue(element.html())
        

        ngModel.$render = ->
          element.html(ngModel.$viewValue || "")
        

        element.bind "blur keyup change", ->
          scope.$apply(read)
    }

  .config ($routeProvider, $locationProvider) ->
    $locationProvider.html5Mode(true);
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'TimelineCtrl'
        controllerAs: 'timeline',
        resolve:
          plugins: ['$ocLazyLoad', ($ocLazyLoad) ->
            $ocLazyLoad.load([
              'scripts/vendor/ionicons/ionicons.css'
              'scripts/vendor/twitter/core.css'
              
              'scripts/vendor/twitter/user.css'
            ])
          ]
      .otherwise
        redirectTo: '/'

