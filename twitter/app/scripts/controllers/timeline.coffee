'use strict'

###*
 # @ngdoc function
 # @name twitterApp.controller:TimelineCtrl
 # @description
 # # TimelineCtrl
 # Controller of the twitterApp
###
angular.module 'twitterApp'
  .controller 'TimelineCtrl', ($scope, websockets) ->
    $scope.tweets = []
    websockets.$on "tweetstream", (tweet) ->
      $scope.tweets.push JSON.parse(tweet)
      $scope.$apply()
        
    $scope.loadTweets = () ->
      websockets.$emit("GET /tweets/latest")
    
    if !$scope.loaded
      $scope.loadTweets()
      $scope.loaded=true
      
    return
