'use strict'

###*
 # @ngdoc function
 # @name twitterApp.controller:AboutCtrl
 # @description
 # # AboutCtrl
 # Controller of the twitterApp
###
angular.module 'twitterApp'
  .controller 'AboutCtrl', ->
    @awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
    return
