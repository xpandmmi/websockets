'use strict'

###*
 # @ngdoc function
 # @name twitterApp.controller:TweetinlineCtrl
 # @description
 # # TweetinlineCtrl
 # Controller of the twitterApp
###
angular.module 'twitterApp'
  .controller 'TweetinlineCtrl', ($scope, websockets)->
    $scope.tweeting = false
    $scope.condensed = true
    $scope.tweet = ""
    $scope.btnDis = true
    
    $scope.remaining = 140
    
    htmlToPlaintext = (text) ->
      String(text).replace(/<[^>]+>/gm, '');
    
    $scope.sendTweet = () ->
      parsedTweet = htmlToPlaintext($scope.tweet)
      if !$scope.btnDis && $scope.tweet
        websockets.$emit("POST /tweets/create", {userid: '7cda0bc0-2eae-11e5-8940-3fc80f078bf9', user: 'fkasper', message: $scope.tweet})
        $scope.btnDis = true
        $scope.tweet = ""
        $scope.condensed = true
        $scope.remaining = 140
      
    $scope.$watch 'tweet', (newValue, oldValue) ->
      parsedTweet = htmlToPlaintext(newValue)
      $scope.remaining = 140 - parsedTweet.length
      if parsedTweet.length > 0
        $scope.btnDis = false
      else
        $scope.btnDis = true

    $scope.closeTweetbox = () ->
      if $scope.tweet.length == 0
        $scope.condensed = true        
      
    $scope.openTweetbox = () ->
      $scope.condensed = false
      # if newValue == true
      #   $scope.condensed = false
      # else
      #   $scope.condensed = true
    
    return
