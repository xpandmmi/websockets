'use strict'

describe 'Controller: TweetinlineCtrl', ->

  # load the controller's module
  beforeEach module 'twitterApp'

  TweetinlineCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TweetinlineCtrl = $controller 'TweetinlineCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(TweetinlineCtrl.awesomeThings.length).toBe 3
