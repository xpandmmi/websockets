'use strict'

describe 'Controller: TimelineCtrl', ->

  # load the controller's module
  beforeEach module 'twitterApp'

  TimelineCtrl = {}

  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TimelineCtrl = $controller 'TimelineCtrl', {
      # place here mocked dependencies
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(TimelineCtrl.awesomeThings.length).toBe 3
