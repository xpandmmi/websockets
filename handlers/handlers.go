package handlers

import (
  "github.com/gocql/gocql"
  "github.com/samuel/go-zookeeper/zk"
  "bitbucket.org/xpandmmi/websockets/services/kafka"
)

type AppLoader struct {
  CassandraSession    *gocql.Session         `inject:""`
  ZookeeperSession    *zk.Conn               `inject:""`
  Kafkas              *kafka.Kafkas          `inject:""`
}