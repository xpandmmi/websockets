package engine

import (
  "net/http"
	"fmt"
  "path"
	//"io/ioutil"
  // "github.com/googollee/go-socket.io"
  log "github.com/Sirupsen/logrus"
  "github.com/rs/cors"
  "github.com/gorilla/mux"
  "bitbucket.org/xpandmmi/websockets/socket"
  //"github.com/gorilla/handlers"
  "bitbucket.org/xpandmmi/websockets/db/files"
  dHandlers "bitbucket.org/xpandmmi/websockets/handlers"
  "github.com/Shopify/sarama"
  "time"
)


func Run(address string, handler dHandlers.AppLoader) error {
	engine := NewEngine(address, handler)
  
	if err := engine.Start(); err != nil {
    log.WithFields(log.Fields{
      "error": "execution_failed",
      "msg": err,
    }).Error("The execution has failed")
    
		return fmt.Errorf("execution failure: %s", err)
	} else {
	}
  return nil
  
}

type Engine struct {
	listenAddress       string
  handler           dHandlers.AppLoader
}

func NewEngine(address string, handler dHandlers.AppLoader) *Engine {
	return &Engine{
		listenAddress:  address,
    handler: handler, 
	}
}


func (e *Engine) Start() error {
  
	if err := e.runSocket(); err != nil {
		return err
	}

  return nil
	// Block until a signal is received or we got an error

}
func (e *Engine) FileServer(w http.ResponseWriter, r *http.Request) {
  w.Header().Set("Server", "XCSS-Operational")
  w.Header().Set("X-XSS-Protection", "1; mode=block")
  w.Header().Set("X-Content-Type-Options", "nosniff")
  w.Header().Set("X-Frame-Options", "deny")
  w.Header().Set("Strict-Transport-Security", "max-age=16070400; includeSubDomains")
  
  log.WithFields(log.Fields{
    "path": r.RequestURI,
    "method": r.Method,
    "remote": r.RemoteAddr,
  }).Debug("Received request")
  
  root := "public/dist"
  if r.URL.Path == "" || r.URL.Path == "/" {
     http.ServeFile(w, r, path.Join(root, "index.html"))
  } else {
     http.ServeFile(w, r, path.Join(root, r.URL.Path))
  }
}


func (e *Engine) runSocket() error {

  r := mux.NewRouter()

  c := cors.New(cors.Options{
      AllowedOrigins: []string{"*"},
      AllowCredentials: true,
  })
  
  sock := socket.NewSocket(&e.handler)
  file := files.NewFiles(&e.handler)
  
  r.Handle("/ws",  c.Handler(sock))
  r.HandleFunc("/files/{created}/{uuid}/{filename}", file.ServeDownload)
  r.HandleFunc("/files/upload", file.ServeUpload)
  //r.HandleFunc("/files/upload", c.Handler(files.Upload))
  r.HandleFunc("/", e.FileServer)
  log.WithFields(log.Fields{
    "port": 3000,
  }).Info("Server is up")
  
  //http.Handle("/", handlers.CompressHandler(handlers.ProxyHeaders(r))))
  //http.Handle("/", r)
  if err := http.ListenAndServe(":3000", e.withAccessLog(r)); err != nil {
    return fmt.Errorf("Unable to start the HTTP listener %s", err)
  }
  return nil
}

func (e *Engine) withAccessLog(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		started := time.Now()

		next.ServeHTTP(w, r)

		entry := &accessLogEntry{
			Method:       r.Method,
			Host:         r.Host,
			Path:         r.RequestURI,
			IP:           r.RemoteAddr,
			ResponseTime: float64(time.Since(started)) / float64(time.Second),
		}

		// We will use the client's IP address as key. This will cause
		// all the access log entries of the same IP address to end up
		// on the same partition.
		e.handler.Kafkas.Async.Input() <- &sarama.ProducerMessage{
			Topic: "access_log",
			Key:   sarama.StringEncoder(r.RemoteAddr),
			Value: entry,
		}
	})
}