package engine

import 	(
  "github.com/stealthly/go-avro"
  "bytes"
)

type accessLogEntry struct {
	Method       string 
	Host         string 
	Path         string 
	IP           string 
	ResponseTime float64

	encoded []byte
	err     error
}

func (ale *accessLogEntry) schema() string {
  return `{
      "namespace": "com.xpand.avro",
      "type": "record",
      "name": "AccessLog",
       "fields": [
         { "name": "method", "type": "string" },
         { "name": "host", "type": "string" },
         { "name": "path", "type": "string" },
         { "name": "ip", "type": "string" },
         { "name": "response_time", "type": "double" }
       ]
  }`
}
func (ale *accessLogEntry) ensureEncoded() {
	schema := avro.MustParseSchema(ale.schema())
  record := avro.NewGenericRecord(schema)
  
  record.Set("method", ale.Method)
  record.Set("host", ale.Host)
  record.Set("path", ale.Path)
  record.Set("ip", ale.IP)
  record.Set("response_time", ale.ResponseTime)
  
  writer := avro.NewGenericDatumWriter()
  writer.SetSchema(schema)
	buffer := new(bytes.Buffer)
	encoder := avro.NewBinaryEncoder(buffer)
  err := writer.Write(record, encoder)
	if err != nil {
		ale.err = err
	}
  
	if ale.encoded == nil && ale.err == nil {
		ale.encoded = buffer.Bytes()
	}
}

func (ale *accessLogEntry) Length() int {
	ale.ensureEncoded()
	return len(ale.encoded)
}

func (ale *accessLogEntry) Encode() ([]byte, error) {
	ale.ensureEncoded()
	return ale.encoded, ale.err
}