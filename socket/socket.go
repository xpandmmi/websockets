package socket

import (
  //"github.com/googollee/go-socket.io"
  log "github.com/Sirupsen/logrus"
  "net/http"
  "github.com/gorilla/websocket"
  //"io"
  "bitbucket.org/xpandmmi/websockets/connection"
  "bitbucket.org/xpandmmi/websockets/handlers"
  
)

type Socket struct {
  upgrader  websocket.Upgrader
  headers   http.Header
}

func NewSocket(loader *handlers.AppLoader) * Socket {
  upgrader := websocket.Upgrader{
      ReadBufferSize:  1024,
      WriteBufferSize: 1024,
      CheckOrigin: func(r *http.Request) bool { return true },
  }
  wsHeaders := http.Header{
      "Sec-WebSocket-Protocol": {"websocket"},
  }
  
  go connection.H.Run(loader)
  
  return &Socket{
    upgrader: upgrader,
    headers: wsHeaders,
  }
}

func (s *Socket) ServeHTTP(w http.ResponseWriter, r *http.Request) {
  
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
  
	ws, err := s.upgrader.Upgrade(w, r, s.headers)
	if err != nil {
    log.WithFields(log.Fields{
      "error": err,
    }).Error("Error starting socket listener")
		return
	}

	c := &connection.Connection{Send: make(chan []byte, 256), Ws: ws }

  connection.H.Register <- c
  go c.WritePump()
  c.ReadPump()
}