package responses


import (
  "encoding/json"
)

type StatusEvent struct {
  key string
  value string
}

func ErrorEvent(err error) []byte {
  event := &StatusEvent{
    key: "Error",
    value: err.Error(),
  }
  eventMarshal, err := json.Marshal(event)
  if err != nil {
    return []byte{}
  }
  return eventMarshal
}

func RenderStatus(key string, value string) []byte{
  event := &StatusEvent{
    key: key,
    value: value,
  }
  
  eventMarshal, err := json.Marshal(event)
  if err != nil {
    return ErrorEvent(err)
  }
  return eventMarshal
}

func OKWithString(text string) []byte {
  return RenderStatus("ok", text)
}

func KafkaEnqueued() []byte {
  return OKWithString("Your message was enqueued into our message broker.")
}