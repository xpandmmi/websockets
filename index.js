//ENV: PORT | ZK_HOSTS | CASS_PEER
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
var kafka = require('kafka-node'),
    HighLevelProducer = kafka.HighLevelProducer,
    client = new kafka.Client(process.env.ZOOKEEPER_SERVICE_HOST),
 //    HighLevelConsumer = kafka.HighLevelConsumer,
 //    consumer = new HighLevelConsumer(
 //     client,
 //     [
 //         { topic: 'clientcontrol' },
 //     ],
 //     {
 //         groupId: 'frontend'
 //     }
 // ),
  producer = new HighLevelProducer(client),
  cassandra = require('cassandra-driver'),
  cqlclient = new cassandra.Client({ contactPoints: [process.env.CASSANDRA_SERVICE_HOST], keyspace: process.env.CASSANDRA_SCHEMA});
    
server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public/dist'));


producer.on('ready', function () {
  io.on('connection', function (socket) {
    // consumer.on("message", function(msg){
    //   socket.broadcast.emit("control", msg);
    // });
    
    socket.on("get status", function(){
    
    });
    
    // persist tweet
    socket.on("send tweet", function(tweet){
      var fTweet = { user_id: tweet.user.id, tweet_content: tweet.tweet, timestamp: Math.floor(new Date() / 1000)};
      var payloads = [
        { topic: 'tweet', messages: fTweet, attributes: 2 }
      ];
      producer.send(payloads, function (err, data) {
        if(!err){
          socket.send("persisted", tweet);
        } else {
          socket.send("error", err);
        }
      });
    });
    
    
    socket.on("get files", function(){
      cqlclient.stream("SELECT id, username, email, realname, twittername FROM users")
        .on('readable', function () {
        //readable is emitted as soon a row is received and parsed
        var row;
        //r//eq.socket.emit("settings", this.read());
        while (row = this.read()) {
          socket.emit("file", row);
        }
      })
      .on('error', function (err) {
        socket.emit("error", {message: err})
      });
    });
    
    socket.on("get users", function(){
      cqlclient.stream("SELECT id, username, email, realname, twittername FROM users")
        .on('readable', function () {
        //readable is emitted as soon a row is received and parsed
        var row;
        //r//eq.socket.emit("settings", this.read());
        while (row = this.read()) {
          socket.emit("user", row);
        }
      })
      .on('error', function (err) {
        socket.emit("error", {message: err})
      });
    });
  });
});

producer.on('error', function (err) {
  
})
