package kafka

import (
  "github.com/Shopify/sarama"
  "time"
  "github.com/samuel/go-zookeeper/zk"
  "encoding/json"
  log "github.com/Sirupsen/logrus"
  "strconv"
)


type Kafkas struct {
  Sync   sarama.SyncProducer
  Async  sarama.AsyncProducer
  connectionStrings []string
}

func NewKafka(zkConn *zk.Conn) *Kafkas{
  k := &Kafkas{
    connectionStrings: getZkStrings(zkConn),
  }
  log.Debug("initializing kafka")
  
  if zkConn != nil{
    k.SetSync()
    k.SetAsync()
  }
  
  return k
}

type BrokerJson struct {
  JmxPort   int     `json:"jmx_port"`
  Timestamp string  `json:"timestamp"`
  Host      string  `json:"host"`
  Version   int     `json:"version"`
  Port      int     `json:"port"`
}

func getZkStrings(zk *zk.Conn) []string {
	children, _, err := zk.Children("/brokers/ids")
    //
  log.WithFields(log.Fields{
    "children": children,
  }).Debug("ZK Result")
  if err != nil {
    panic(err)
  }
  connections := []string{}
  for _, value := range children {
    broker := &BrokerJson{}
    jsonData, _, err := zk.Get("/brokers/ids/"+value)
    if err != nil {
      panic(err)
    }
    if err := json.Unmarshal(jsonData, &broker); err != nil {
      log.WithFields(log.Fields{
        "error": err,
      }).Error("Panic during JSON deserialization")
      panic("Kafka connection strings")
    }
    connections = append(connections, broker.Host+":"+strconv.Itoa(broker.Port))

  }
  log.WithFields(log.Fields{
    "connections": connections,
  }).Debug("Gathering Broker")
  if(len(connections) < 1) {
    panic("No Kafkas available. Are you sure they are connected to Zookeeper?")
  }
  return connections
}

func (k *Kafkas) PublishSync(topic string, content sarama.Encoder) (int32, int64, error) {
  return k.Sync.SendMessage(&sarama.ProducerMessage{
		Topic: topic,
		Value: content,
	})
}


func (k *Kafkas) SetSync() {

	// For the data collector, we are looking for strong consistency semantics.
	// Because we don't change the flush settings, sarama will try to produce messages
	// as fast as possible to keep latency low.
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll // Wait for all in-sync replicas to ack the message
	config.Producer.Retry.Max = 10                   // Retry up to 10 times to produce the message

	// On the broker side, you may want to change the following settings to get
	// stronger consistency guarantees:
	// - For your broker, set `unclean.leader.election.enable` to false
	// - For the topic, you could increase `min.insync.replicas`.

	producer, err := sarama.NewSyncProducer(k.connectionStrings, config)
	if err != nil {
    panic(err)
	}

	k.Sync = producer
}

func (k *Kafkas) SetAsync() {

	// For the access log, we are looking for AP semantics, with high throughput.
	// By creating batches of compressed messages, we reduce network I/O at a cost of more latency.
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms

	producer, err := sarama.NewAsyncProducer(k.connectionStrings, config)
	if err != nil {
		panic(err)
	}

	// We will just log to STDOUT if we're not able to produce messages.
	// Note: messages will only be returned here after all retry attempts are exhausted.
	go func() {
		for err := range producer.Errors() {
			panic(err)
		}
	}()

	k.Async = producer
}