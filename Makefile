ETCD_NODE1 := http://127.0.0.1:4001
ETCD_NODE2 := http://127.0.0.1:4002
ETCD_NODE3 := http://127.0.0.1:4003
ETCD_NODES := ${ETCD_NODE1},${ETCD_NODE2},${ETCD_NODE3}


API_URL := http://localhost:8282


all: build


build:
	go build -a -tags netgo -installsuffix cgo -ldflags '-w' -o ./xpws

install:
	go install bitbucket.org/xpandmmi/websockets