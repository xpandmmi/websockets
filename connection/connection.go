package connection

import (
  //"github.com/googollee/go-socket.io"
  log "github.com/Sirupsen/logrus"
  //"net/http"
  //"github.com/gocql/gocql"
  "github.com/gorilla/websocket"
  "encoding/json"
  "time"
  "bitbucket.org/xpandmmi/websockets/db/users"
  "bitbucket.org/xpandmmi/websockets/db/tweets"
  
)

type Connection struct {
	Ws *websocket.Conn
	Send chan []byte
}




const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)



type wsMessage struct {
  Event string `json:"event"`
  Data  string `json:"data"`
}
type wsInMessage struct {
  Event string `json:"event"`
  Data  json.RawMessage `json:"data"`
}

type BrowserEvent struct {
  Action string `json:"action"`
  When time.Time `json:"when"`
}

func (c *Connection) ErrorEvent() []byte{
  data, _ := json.Marshal(&wsMessage{
    Event: "error",
    Data: "An unexpected error occured",
  })
  return data
}

func (c *Connection) SpecifiedError(err error) []byte{
  data, _ := json.Marshal(&wsMessage{
    Event: "error",
    Data: err.Error(),
  })
  return data
}

func (c *Connection) respondWithJson(eventtype string, data string) {
  event := &wsMessage{
      Event: eventtype, 
      Data: data,
  }
  eventMarshal, err := json.Marshal(event)
  if err != nil {
    c.write(websocket.TextMessage, c.ErrorEvent())
    return
  }
  c.write(websocket.TextMessage, eventMarshal)
}

func (c *Connection) ReadPump() {
	defer func() {
		H.Unregister <- c
		c.Ws.Close()
	}()
	c.Ws.SetReadLimit(maxMessageSize)
	c.Ws.SetReadDeadline(time.Now().Add(pongWait))
	c.Ws.SetPongHandler(func(string) error { c.Ws.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.Ws.ReadMessage()
		if err != nil {
			break
		}

    m := wsInMessage{}
    if err := json.Unmarshal(message, &m); err != nil {
      log.WithFields(log.Fields{
        "error": err,
      }).Warn("JSON unmarshal error")
      c.write(websocket.TextMessage,c.SpecifiedError(err))
      continue
    }
    
    if m.Event == "" {
      continue
    }
    
    log.WithFields(log.Fields{
      "event": m.Event,
      "data": m.Data,
    }).Debug("Received valid routing message")
    
    userData := users.NewUsers(H.connection)
    switch m.Event {
      case "GET /users":
        userData.GetAll(func(userjson []byte){
          c.respondWithJson("user", string(userjson))
        })
      case "POST /tweets/create":
        twitter := tweets.NewTwitter(H.connection)
        twitter.PostTweet(m.Event, m.Data, func(response []byte){
          c.respondWithJson("tweet", string(response))
        })
      case "GET /tweets/latest":
        twitter := tweets.NewTwitter(H.connection)
        twitter.LatestTweets(30, func(response []byte){
          c.respondWithJson("tweetstream", string(response))
        })
      default:
        c.respondWithJson("error", string(c.ErrorEvent()))
    }
    
    //c.Router()
    // log.WithFields(log.Fields{
    //   "event": m.Event,
    //   "data": m.Data,
    // }).Debug("Received message")
    // userData := users.NewUsers(H.connection)
    //
    // switch m.Event {
    //   case "post tweet":
    //     twitter := tweets.NewTwitter(H.connection)
    //     twitter.PostTweet(m.Event, m.Data, func(response []byte){
    //       log.Info("Sending out tweet")
    //
    //       c.respondWithJson("tweet", string(response))
    //     })
    //   case "get users":
    //     userData.GetAll(func(userjson []byte){
    //       c.respondWithJson("user", string(userjson))
    //     })
    //   default:
    //     c.respondWithJson("error", string(c.ErrorEvent()))
    // }
    //c.write(websocket.TextMessage, message)
		//H.broadcast <- message
	}
}

func (c *Connection) write(mt int, payload []byte) error {
	c.Ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.Ws.WriteMessage(mt, payload)
}

func (c *Connection) WritePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.Ws.Close()
	}()
	for {
		select {
		case message, ok := <-c.Send:
			if !ok {
				c.write(websocket.CloseMessage, []byte{})
				return
			}
			if err := c.write(websocket.TextMessage, message); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}