package connection

import "bitbucket.org/xpandmmi/websockets/handlers"


// hub maintains the set of active connections and broadcasts messages to the
// connections.
type Hub struct {
	// Registered connections.
	Connections map[*Connection]bool

	// Inbound messages from the connections.
	Broadcast chan []byte

	// Register requests from the connections.
	Register chan *Connection

	// Unregister requests from connections.
	Unregister chan *Connection
  
  connection *handlers.AppLoader
}

var H = Hub{
	Broadcast:   make(chan []byte),
	Register:    make(chan *Connection),
	Unregister:  make(chan *Connection),
	Connections: make(map[*Connection]bool),
}

func (H *Hub) Run(loader *handlers.AppLoader) {
  H.connection = loader
	for {
		select {
		case c := <-H.Register:
			H.Connections[c] = true
		case c := <-H.Unregister:
			if _, ok := H.Connections[c]; ok {
				delete(H.Connections, c)
				close(c.Send)
			}
		case m := <-H.Broadcast:
			for c := range H.Connections {
				select {
				case c.Send <- m:
				default:
					close(c.Send)
					delete(H.Connections, c)
				}
			}
		}
	}
}