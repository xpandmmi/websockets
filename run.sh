#!/bin/bash

echo  "   _____ ______________  __________   "
echo  "  / ___//  _/_  __/ __ \/ ____/ __ \  "
echo  "  \__ \ / /  / / / /_/ / __/ / /_/ /  "
echo  " ___/ // /  / / / _, _/ /___/ ____/   "
echo  "/____/___/ /_/ /_/ |_/_____/_/        "

ETCD_PEER="http://$ETCD_PORT_2379_TCP_ADDR:$ETCD_PORT_2379_TCP_PORT"
CASSANDRA_PEERS=`env | grep CASS[0-9]_PORT_9042_TCP_ADDR | sed 's/CASS[0-9]_PORT_9042_TCP_ADDR=//g' | sed -e :a -e N -e 's/\n/-cassandra=/' -e ta`

if [ -z "$CASSANDRA_PEERS" ]; then
  CASSANDRA_PEERS=$CASSANDRA_SERVICE_HOST
fi

echo "Starting up sitrep. ETCD url: $ETCD_PEER"

/go/bin/websockets -peers=$ETCD_PEER \
    $CASSANDRA_PEERS \
    -zookeeper=$ZOOKEEPER_SERVICE_HOST \
    -keyspace=xcms